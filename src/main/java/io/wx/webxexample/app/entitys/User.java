/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.webxexample.app.entitys;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.wx.modules.mongo2.entity.CRUDEntity;
import io.wx.modules.mongo2.entity.Entity;

/**
 *
 * @author moritz
 */
@Entity(collection = "User")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends CRUDEntity{
    private String name;
    private String vorname;
    private String email;

    public User() {
    }

    public User(String name, String vorname, String email) {
        this.name = name;
        this.vorname = vorname;
        this.email = email;
    }

    
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
    
    
}
