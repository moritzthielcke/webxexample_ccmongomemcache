/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.webxexample.app.ctr;

import io.wx.core.HTTP.GET;
import io.wx.core.HTTP.HttpController;
import io.wx.core.HTTP.Resource;
import io.wx.core.cache.Cache;
import io.wx.core.cache.CacheNotFoundException;
import io.wx.core.cdi.l4j.InjectLogger;
import io.wx.webxexample.app.entitys.User;
import javax.inject.Inject;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Resource(path = "/hello")
public class HelloWorld {
    @Inject HttpController ctr;
    @InjectLogger Logger logger;
    @Inject Cache cache;
    
    @GET
    public User hw(){
        String name = (ctr.getRequest().params().get("name") != null) ?  ctr.getRequest().params().get("name") : "hans";
        String lastName = (ctr.getRequest().params().get("lastname") != null) ?  ctr.getRequest().params().get("lastname") : "wurst";
        User u = new User(name, lastName, name+lastName+"@aptly.de");
        return u;
    }
    
    @GET(path = "/cache")
    public String helloCache() throws CacheNotFoundException{
        String val;
        if((val = ctr.getRequest().params().get("push")) != null){
            cache.put("testcache", "testvalue", val);
            return "ok => "+val;
        }
        else if(ctr.getRequest().params().get("pop") != null){
            return (String) cache.get("testcache", "testvalue");          
        }
        return "?";
    }
    
    
}
