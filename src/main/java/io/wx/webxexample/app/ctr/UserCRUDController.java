/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.webxexample.app.ctr;

import io.wx.core.HTTP.Resource;
import io.wx.modules.mongo2.crud.CRUDController;
import io.wx.webxexample.app.entitys.User;

/**
 *
 * @author moritz
 */
@Resource(path = "/user")
public class UserCRUDController extends CRUDController<User>{
    
}
