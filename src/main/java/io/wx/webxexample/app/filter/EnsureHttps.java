/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.webxexample.app.filter;

import io.wx.core.HTTP.HttpController;
import io.wx.core.HTTP.filter.Before;
import io.wx.core.HTTP.filter.Filter;
import io.wx.core.cdi.l4j.InjectLogger;
import io.wx.core.config.AppConfig;
import javax.inject.Inject;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Filter(path = "/.*")
public class EnsureHttps {
    @Inject HttpController ctr;
    @InjectLogger Logger log;
    @Inject AppConfig cfg;

    
    @Before
    public Boolean ensureHttps(){
        if(!ctr.getRequest().absoluteURI().toString().startsWith("https") && 
           !"https".equals(ctr.getRequest().headers().get("X-FORWARDED-PROTO")))         
        {
             ctr.getRequest().response().headers().add("Location", "https://"+ctr.getRequest().headers().get("Host")+ctr.getRequest().uri());
             ctr.getRequest().response().setStatusCode(302);
             return false; 
        }
        return true;
    }
    
}
