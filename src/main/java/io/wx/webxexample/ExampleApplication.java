package io.wx.webxexample;
import io.wx.core.config.AppConfig;
import io.wx.core.config.CloudConfigurator;
import io.wx.core.config.SimpleConfig;
import io.wx.modules.mongo2.MongoApp;


/**
 *
 * @author moritz
 */
public class ExampleApplication extends MongoApp{
    //our app config
    private static final AppConfig cfg = new SimpleConfig().setInstanceCount(3).setSSL(true).setPort(8888);
    
    public static void main(String[] args) throws Exception {
        ExampleApplication app = new ExampleApplication();
        app.start();
        app.getCache().addCache("testcache");
        app.idle();
    }
    
    public ExampleApplication() throws Exception{        
        super(new CloudConfigurator(cfg));
    }
    
    @Override
    public String getApplicationPath() {
        return "io.wx.webxexample.app";
    }
    
}
