# Webx on CloudControl with MongoDB (mongosoup as provider) and optional memcache. #

## Setup Cloud Control: ##
Go to cloudcontrol.com
Follow https://www.cloudcontrol.com/dev-center/quickstart until you reach the section *Create the First Application on cloudControl*
Stop at this point.

## Checkout and build this Project ##

This project requires you to checkout and build
*  https://bitbucket.org/moritzthielcke/webx.io (branch develop)
*  https://bitbucket.org/moritzthielcke/vertx-mongo-persistor (master)
*  https://bitbucket.org/moritzthielcke/webx-mongomodule2 (develop)

We are working on a public maven, to make this step obsolete in the future!

After building those 3 dependencies , you should be good to go to build the example project!

After building the project run  io.wx.webxexample.ExampleApplication in your IDE or the navigate to target/deploy and run the generated jar file.

The application should now run and greet you under https://localhost:8888/hello !


##  Create your Application ##
Create an CloudControl Application ether over https://www.cloudcontrol.com/console or per command line:
cctrlapp APP_NAME create java

Add the Mongosoup Addon:
 cctrlapp app_name/default addon.add mongosoup.sandbox

Optional add Memcache (@todo)


## deploy into the cloud ##
create a dedicated deployment directory somewhere outside of the actual project directory

copy everything from your projectDirectory /target/deploy into your deployment directory
( cp -r myProject/target/deploy/* myProjectDepoyment/ )

Add all the whole directory to git
(git add -A , git commit -am "init")

push your deployment
(cctrlapp APP_NAME/default push)

everything ok? then deploy!
(cctrlapp APP_NAME/default deploy)
 

Check your log file with
(cctrlapp APP_NAME/default log error)

Login into "one" of your cloudlets with
(cctrlapp APP_NAME/default run bash)


Check your app under
app_name.cloudcontrolled.com !

Optional add the remote repository from cloudcontrol:
git remote add origin ssh://yourapp/repository.git